@extends('layout')

@section('title')
<title>Home</title>
@endsection

<style>
    .color{
        margin-left:5px;
    }

    .notes:hover{
        box-shadow: 3px 5px 9px #d1d1d1;
        border: none !important;
    }

    .notes:hover p{
        color:black !important;
    }

    .list-note:hover{
        box-shadow: 3px 5px 9px #d1d1d1;
        border: none !important;
    }

    .notes:hover .content{
        font-weight:bold;
    }
    .note-butt{
        background-image: linear-gradient(to right, #55cf75, #db736b, #b685de,#e0bb53,#e08a2d) !important;
        border:none !important;
        font-weight:bold !important;
        box-shadow: 3px 5px 9px #d9d9d9;
    }

    .note-butt:hover{
        box-shadow: 3px 5px 9px #bababa;
    }

    .note-footer{
        display:grid;
        grid-template-columns: 1fr 1fr;
    }
</style>

@section('content')

<div>
    <div class='row' style='margin-top:10px;'>
        <div class="col">
            <button class='btn btn-info note-butt' data-toggle='modal' data-target='#note-modal'>New Note</button>
        </div>
        <div class="col">
            <form action="" method='post'>
                <select class='form-control taggy' id="">
                    <option style='font-weight:bold !important;' selected value="">All notes</option>
                    @foreach($taggg as $t)
                        <option class='option' value="{{$t->tag}}">{{$t->tag}}</option>
                    @endforeach
                </select>
                @csrf
            </form>
            <img class='float-right list' src="/images/test.png" style='width:25px;height:25px;' alt="">
            <img class='float-right block' src="/images/block.png" style='width:25px;height:25px;position:absolute;top:-9999px; left:-9999px' alt="">
        </div>
    </div>

    <div class="modal fade" style='opacity: .9;border:none;' id="note-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class='modal-content'>
                <div class='form-group' style='margin:25px;'>
                    <form action="" method='post'>
                        <input class='form-control title' type="text" name='title' placeholder='Title'><br>
                        <textarea class='form-control' name="notes" placeholder='Notes...' id="" cols="30" rows="10"></textarea><br>
                        <input class='form-control note-color' type="text" name='color' style='display:none;'>
                        
                        <input class='form-control tag' type="text" name='tag' placeholder='tag'><br>

                        <div class='row'>
                            <div class="col-2"><button type="submit" class='btn btn-info save-btn'>Save</button></div>
                            <div class="col-10">
                                <div class='color green float-right' style='height:50px; width: 50px; background-color: #55cf75;border-radius:100px;'></div>
                                <div class='color red float-right' style='height:50px; width: 50px; background-color: #db736b;border-radius:100px;'></div>
                                <div class='color purple float-right' style='height:50px; width: 50px; background-color: #b685de;border-radius:100px;'></div>
                                <div class='color yellow float-right' style='height:50px; width: 50px; background-color: #e0bb53;border-radius:100px;'></div>
                                <div class='color yellow float-right' style='height:50px; width: 50px; background-color: #e08a2d;border-radius:100px;'></div>
                            </div>
                        </div>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class='user-notes row border border-light' style='padding:5px;margin:10px;border-radius:10px;'>
    @foreach($notes as $note)
        <div class='border col-lg-3 notes {{$note->tag}}' style="border-radius: 5px; width:250px;height:200px;margin:10px;background-color:{{$note->color}}; position:relative;">
            <h5 class='text-dark'>{{ $note->title }}</h5>
            <div class='content' style='margin:10px;overflow-y:auto; height:100px;'>
                {{ $note->content }}
            </div>
            <div class='note-footer' style='position:absolute; bottom:0;'>
                <div>
                    <p class='text-secondary' style='font-size: 12px;'>{{ $note->updated_at }}</p> 
                </div>
                <div>
                    <p class='note-tag' style='font-size:12px; background-color:lightgray;padding:5px;border-radius:5px;margin-left:25px;'> {{ $note->tag }} </p>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class='border border-light list-view' style='padding:5px;margin:10px;border-radius:10px; display:none;'>
    @foreach($notes as $note)
        <div class='border list-note' style="border-radius: 5px; padding:5px !important; padding:2px;margin:10px; position:relative;background-color:{{$note->color}};">
            <h5 class='text-dark'>{{ $note->title }}</h5>
            <div class='content' style='margin:10px;overflow-y:auto; height:100px;'>
                {{ $note->content }}
            </div>
            <div class='note-footer' style='position:absolute; bottom:0;'>
                <div>
                    <p class='text-secondary' style='font-size: 12px;'>{{ $note->updated_at }}</p> 
                </div>
                <div>
                    <p class='note-tag' style='font-size:12px; background-color:lightgray;padding:5px;border-radius:5px;margin-left:25px;'> {{ $note->tag }} </p>
                </div>
            </div>
        </div>
    @endforeach
</div>
@endsection

@section('script')
    <script>
        $(".color").hover(function(){
            var color = $(this).css("background-color");
            $('textarea').css("background-color", color);
            $('textarea').css("border-color", color);
            $('.title').css("border-color", color);
            $('.save-btn').css("border-color", color);
            $('.save-btn').css("background-color", color);
            $('.note-color').val(color);
            $('.tag').css('border-color', color);
            console.log($('.note-color').val())
            console.log(color);
        });
        $(".notes").hover(function(){
            var color = $(this).css("background-color");
            $('.taggy').css('border-color', color);
            console.log(color);
        });
        $(".list-note").hover(function(){
            var color = $(this).css("background-color");
            $('.taggy').css('border-color', color);
            console.log(color);
        });
        $(".list").click(function(){
            $('.list-view').css('display', 'block');
            $('.user-notes').css('position', 'absolute');
            $('.user-notes').css('top', '-9999px');
            $('.user-notes').css('left', '-9999px');
            $('.block').css('position', 'static');
            $('.block').css('top', '9999px');
            $('.block').css('left', '9999px');
            $('.list').css('position', 'absolute');
            $('.list').css('top', '-9999px');
            $('.list').css('left', '-9999px');
        });
        $(".block").click(function(){
            $('.list-view').css('display', 'none');
            $('.user-notes').css('position', 'static');
            $('.user-notes').css('top', '9999px');
            $('.user-notes').css('left', '9999px');
            $('.list').css('position', 'static');
            $('.list').css('top', '9999px');
            $('.list').css('left', '9999px');
            $('.block').css('position', 'absolute');
            $('.block').css('top', '-9999px');
            $('.block').css('left', '-9999px');
        });


        $('.taggy').change(function(){
            var tag = $(this).val();
            console.log(tag);

            var note_count = $('.notes').length;

            $('.notes').each(function(){
                if($(this).hasClass(tag) == FALSE){
                    console.log($(this).attr('class'));
                }
            })
        });

    </script>
@endsection