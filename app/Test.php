<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $guarded = [];

    public function scopeTag($query){
        return $query->select('tag')->groupBy('tag')->distinct();
    }
}
