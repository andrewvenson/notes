<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;

class TestController extends Controller
{
    public function getNotes(){
        $notes = Test::all();
        $taggg = Test::tag()->get();
        return view('home', compact('notes', 'taggg'));
    }

    public function postNotes(){
        $data = request()->validate([
            'title'=>'',
            'content'=>'',
            'color'=>'',
            'tag'=>'',
        ]);
        
        $note= Test::create($data);

        return back();
    }

}
